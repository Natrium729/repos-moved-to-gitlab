# My repos are now on GitLab

If you are looking for my repos, [they are now on GitLab](https://gitlab.com/Natrium729).

## Why?

Bitbucket no longer supports Mercurial repositories, will delete them in June 2020, and won't provide an option to convert them to Git. Since most of my repos used Mercurial,  I had to convert them to Git. I could have then kept my repos on Bitbucket, but I no longer trust them, so I've decided to switch to Gitlab.

Also, since GitHub and GitLab now offer illimited private repositories, I don't think there are any reasons left to stay on Bitbucket.